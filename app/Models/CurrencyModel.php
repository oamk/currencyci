<?php namespace App\Models;

use CodeIgniter\Model;

class CurrencyModel extends Model {
  public function convert($eur) {
    return $eur * 0.89;
  }
}