<?php namespace App\Controllers;

use App\Models\CurrencyModel;

class Currency extends BaseController
{
	public function index()
	{
		return view('currency_view');
  }
  
  public function calculate() {
    $currency_model = new CurrencyModel();
    $eur = $this->request->getVar('eur');
    $data['eur'] = $eur;
    $data['gbp'] = $currency_model->convert($eur);
    return view ('calculate_view',$data);
  }

	//--------------------------------------------------------------------

}